require './model/car'
require './model/game'
require './model/move'
require './model/world'
require 'ostruct'

class MyStrategy
  # @param [Car] me
  # @param [World] world
  # @param [Game] game
  # @param [Move] move

  attr_reader :me
  attr_reader :world
  attr_reader :game
  attr_reader :my_move

  attr_reader :prev_coords
  attr_reader :cur_coords
  attr_reader :reverse_ticks

  attr_reader :next_waypoint_x_cache
  attr_reader :next_waypoint_y_cache
  attr_reader :prev_waypoint_x_cache
  attr_reader :prev_waypoint_y_cache

  attr_reader :waypoints_cache

  def waypoint_changed?
    next_waypoint_x_cache != prev_waypoint_x_cache || next_waypoint_y_cache != prev_waypoint_y_cache
  end

  def moved_dist
    return 100 if !cur_coords || !prev_coords
    Math.hypot(cur_coords.x-prev_coords.x, cur_coords.y-prev_coords.y)
    # 100
  end

  def stuck?
    return false if world.tick < game.initial_freeze_duration_ticks + 3
    moved_dist < 0.1 && !in_reverse_mode?
  end

  def in_reverse_mode?
    reverse_ticks && reverse_ticks > 0
  end

  def pi
    Math::PI
  end

  def my_tile_x
    dist_to_tiles(me.x).to_i
  end

  def my_tile_y
    dist_to_tiles(me.y).to_i
  end

  def calc_path
    if waypoint_changed?
      astar = Astar.new(
        {'x' => my_tile_x, 'y' => my_tile_y },
        {'x' => me.next_waypoint_x, 'y' => me.next_waypoint_y},
        world
      )
      @waypoints_cache = astar.search
    end
    waypoints_cache
  end

  def current_wp_tile
    tile = calc_path.find {|tile| tile.x != my_tile_x || tile.y != my_tile_y}
    puts "my tile x: #{my_tile_x}, y: #{my_tile_y}"
    puts "x: #{tile.x}, y: #{tile.y}"
    tile
  end

  def get_next_waypoint_x
    (current_wp_tile.x + 0.5) * game.track_tile_size
  end

  def get_next_waypoint_y
    (current_wp_tile.y + 0.5) * game.track_tile_size
  end

  def get_angle_to_waypoint
    next_waypoint_x = get_next_waypoint_x
    next_waypoint_y = get_next_waypoint_y
    me.get_angle_to(next_waypoint_x, next_waypoint_y)
  end

  def dist_tiles(n)
    game.track_tile_size * n
  end

  def dist_to_tiles(dist)
    dist / dist_tiles(1)
  end

  def deg_to_rad(n)
    Math::PI/180 * n
  end

  def register_move
    @prev_waypoint_x_cache = next_waypoint_x_cache
    @prev_waypoint_y_cache = next_waypoint_y_cache
    @next_waypoint_x_cache = me.next_waypoint_x
    @next_waypoint_y_cache = me.next_waypoint_y
    to_remove = calc_path.find {|tile| tile.x == my_tile_x && tile.y == my_tile_y}
    @waypoints_cache -= [to_remove]
    cur = make_coords(me.x, me.y)
    @prev_coords = cur_coords
    @cur_coords = cur
  end

  def distance_to_waypoint
    me.distance_to(get_next_waypoint_x, get_next_waypoint_y)
  end

  def calc_waypoint
    next_waypoint_x = get_next_waypoint_x
    next_waypoint_y = get_next_waypoint_y

    distance_coef = distance_to_waypoint > dist_tiles(1.4) ? 0 : 0.8

    corner_tile_offset = distance_coef * game.track_tile_size
    waypoint_type = world.tiles_x_y[me.next_waypoint_x][me.next_waypoint_y]

    case waypoint_type
    when TileType::LEFT_TOP_CORNER
      next_waypoint_x += corner_tile_offset
      next_waypoint_y += corner_tile_offset
    when TileType::RIGHT_TOP_CORNER
      next_waypoint_x -= corner_tile_offset
      next_waypoint_y += corner_tile_offset
    when TileType::LEFT_BOTTOM_CORNER
      next_waypoint_x += corner_tile_offset
      next_waypoint_y -= corner_tile_offset
    when TileType::RIGHT_BOTTOM_CORNER
      next_waypoint_x -= corner_tile_offset
      next_waypoint_y -= corner_tile_offset
    end
    return next_waypoint_x, next_waypoint_y
  end

  def move(me, world, game, move)
    @me = me
    @world = world
    @game = game
    @my_move = move

    register_move
    if stuck? || in_reverse_mode?
      return unless recover_from_stop
    end

    next_waypoint_x, next_waypoint_y = calc_waypoint

    angle_to_waypoint = me.get_angle_to(next_waypoint_x, next_waypoint_y)
    speed_module = Math.hypot(me.speed_x, me.speed_y)

    turn = angle_to_waypoint
    move.wheel_turn = turn
    move.engine_power = 1
    if distance_to_waypoint > dist_tiles(4)
      my_move.use_nitro = true
    end

    magic = 10

    if speed_module * speed_module * angle_to_waypoint.abs > magic * magic * pi
      move.brake = true
    end

    attack  
    pick_bonuses
    # avoid_obstacle
  end

  def make_coords(x, y)
    OpenStruct.new({x: x, y: x})
  end

  def tile_neighbours(tile)
    [
      make_coords(tile.x + 1, tile.y + 1),
      make_coords(tile.x + 1, tile.y - 1),
      make_coords(tile.x + 1, tile.y),
      make_coords(tile.x - 1, tile.y + 1),
      make_coords(tile.x - 1, tile.y - 1),
      make_coords(tile.x - 1, tile.y),
      make_coords(tile.x, tile.y + 1),
      make_coords(tile.x, tile.y)
    ].map do |tile|
      tile.type = tile_type(tile)
      tile
    end
  end

  def tile_type(tile)
    world.tiles_x_y[tile.x][tile.y] rescue TileType::EMPTY
  end

  def avoid_obstacle
    x = me.x
    y = me.y
    speed_x = me.speed_x
    speed_y = me.speed_y
    next_tile_x = dist_to_tiles(x + speed_x)
    next_tile_y = dist_to_tiles(y + speed_y)
    next_tile_type = world.tiles_x_y[next_tile_x][next_tile_y]
    return unless next_tile_type == TileType::EMPTY

    my_tile_x = dist_to_tiles(x)
    my_tile_y = dist_to_tiles(y)

    neighbours = tile_neighbours(make_coords(my_tile_x, my_tile_y))
    not_empty_neighbours = neighbours.select {|tile| tile.type != TileType::EMPTY}
    not_empty_neighbours.each do |tile|
      wp_x = me.next_waypoint_x
      wp_y = me.next_waypoint_y
      dist_x = wp_x - tile.x
      dist_y = wp_y - tile.y
      dist = Math.hypot(dist_x, dist_y)
      tile.dist_to_wp = dist
    end
    not_empty_neighbours.sort! do |a, b|
      a.dist - b.dist
    end
    closest = not_empty_neighbours.first
    my_move.wheel_turn = me.angle_to(closest.x, closest.y)
  end

  REVERSE_TICKS_COUNT = 2

  def recover_from_stop 
    if in_reverse_mode?
      @reverse_ticks -= 1
    else
      @reverse_ticks = REVERSE_TICKS_COUNT
    end 
    if in_reverse_mode?
      # if me.engine_power > 0
      my_move.engine_power = -1 
      next_waypoint_x, next_waypoint_y = calc_waypoint
      angle_to_waypoint = me.get_angle_to(next_waypoint_x, next_waypoint_y)
      my_move.wheel_turn = -angle_to_waypoint

      # else
      #   my_move.engine_power = 1 
      # end
      # my_move.wheel_turn = 0
      false
    else
      true
    end
  end

  def attack
    world.cars.each do |car|
      next if car.teammate
      distance = me.get_distance_to_unit(car)
      next if distance > attack_distance
      angle = me.get_angle_to_unit(car)
      if angle.abs < attack_washer_angle
        my_move.throw_projectile = true
      end
      if angle.abs > attack_oil_angle
        my_move.spill_oil = true
      end
    end
  end

  def pick_bonuses
    distance_to_wp = me.distance_to(get_next_waypoint_x, get_next_waypoint_y)
    return if distance_to_wp < dist_tiles(2)
    world.bonuses.each do |bonus|
      distance = me.get_distance_to_unit(bonus)
      next if distance > bonus_max_pick_distance || distance < bonus_min_pick_distance
      angle = me.get_angle_to_unit(bonus)
      next if angle.abs > bonus_pick_angle(bonus)
      my_move.wheel_turn = angle
      break
    end
  end

  def bonus_pick_angle(bonus)
    # distance_to_wp = me.distance_to(get_next_waypoint_x, get_next_waypoint_y)
    # width = distance_to_wp / dist_tiles(3)
    # width = width > 1 ? 1 : width
    # grad = 7 * width
    grad = 3
    deg_to_rad(grad)
  end

  def bonus_max_pick_distance
    dist_tiles(3)
  end

  def bonus_min_pick_distance
    dist_tiles(1)
  end

  def attack_distance
    dist_tiles(3)
  end

  def attack_washer_angle
    deg_to_rad(3)
  end 

  def attack_oil_angle
    pi - (pi/180 * 5)
  end
end





class Astar

  def initialize(start, destination, world)
    @world = world

    # create start and destination nodes
    @start_node = Astar_Node.new(start['x'],   start['y'],           -1, -1, -1, -1)
    @dest_node  = Astar_Node.new(destination['x'], destination['y'], -1, -1, -1, -1)

    @open_nodes   = [] # conatins all open nodes (nodes to be inspected)
    @closed_nodes = [] # contains all closed nodes (node we've already inspected)

    @open_nodes.push(@start_node) # push the start node
  end

  # calc heuristic
  def heuristic(current_node, destination_node)
    return ( Math.sqrt( ((current_node.x - destination_node.x) ** 2) + ((current_node.y - destination_node.y) ** 2) ) ).floor
  end

  # calc cost
  def cost(current_node, destination_node)
    direction = direction(current_node, destination_node)

    return 10 if [2, 4, 6, 8].include?(direction) # south, west, east, north
    return 14
  end

  # determine direction (2, 4, 6, 8)
  def direction(current_node, destination_node)
    direction = [ destination_node.y - current_node.y,  # down/up
                  destination_node.x - current_node.x ] # negative: left, positive: right

    return 2 if direction[0] > 0 and direction[1] == 0 # south
    return 4 if direction[1] < 0 and direction[0] == 0 # west
    return 8 if direction[0] < 0 and direction[1] == 0 # north
    return 6 if direction[1] > 0 and direction[0] == 0 # east

    return 0 # default
  end

  # field passable?
  def passable?(x, y)
    tile_type = @world.tiles_x_y[x][y] rescue TileType::EMPTY
    tile_type != TileType::EMPTY
  end

  # expand node in all 4 directions
  def expand(current_node)
    x   = current_node.x
    y   = current_node.y

    return [ [x, (y - 1)],  # north
             [x, (y + 1)],  # south
             [(x + 1), y],  # east
             [(x - 1), y] ] # west
  end

  def search
    while @open_nodes.size > 0 do
      # grab the lowest f(x)
      low_i = 0
      for i in 0..@open_nodes.size-1
        if @open_nodes[i].f < @open_nodes[low_i].f then
          low_i = i
        end
      end
      best_node = low_i

      # set current node
      current_node = @open_nodes[best_node]

      # check if we've reached our destination
      if (current_node.x == @dest_node.x) and (current_node.y == @dest_node.y) then
        path = [@dest_node]

        # recreate the path
        while current_node.i != -1 do
          current_node = @closed_nodes[current_node.i]
          path.unshift(current_node)
        end

        return path
      end

      # remove the current node from open node list
      @open_nodes.delete_at(best_node)

      # and push onto the closed nodes list
      @closed_nodes.push(current_node)

      # expand the current node
      neighbor_nodes = expand(current_node)
      for n in 0..neighbor_nodes.size-1
        neighbor = neighbor_nodes[n]
        nx       = neighbor[0]
        ny       = neighbor[1]

        # if the new node is passable or our destination
        if (passable?(nx, ny) or (nx == @dest_node.x and ny == @dest_node.y)) then
          # check if the node is already in closed nodes list
          in_closed = false
          for j in 0..@closed_nodes.size-1
            closed_node = @closed_nodes[j]
            if nx == closed_node.x and ny == closed_node.y then
              in_closed = true
              break
            end
          end
          next if in_closed

          # check if the node is in the open nodes list
          # if not, use it!
          in_open = false
          for j in 0..@open_nodes.size-1
            open_node = @open_nodes[j]
            if nx == open_node.x and ny == open_node.y then
              in_open = true
              break
            end
          end

          unless in_open then
            new_node = Astar_Node.new(nx, ny, @closed_nodes.size-1, -1, -1, -1)

            # setup costs
            new_node.set_g(current_node.g + cost(current_node, new_node))
            new_node.set_h(heuristic(new_node, @dest_node))
            new_node.set_f(new_node.g + new_node.h)

            @open_nodes.push(new_node)
          end
        end
      end
    end

    return [] # return empty path
  end

end

# Astar node representation
class Astar_Node

  # x = x-position
  # y = y-position
  # i = parent index
  # g = cost from start to current node
  # h = cost from current node to destination
  # f = cost from start to destination going through the current node
  def initialize(x, y, i, g, h, f)
    @x = x
    @y = y
    @i = i
    @g = g
    @h = h
    @f = f
  end

  def x
    return @x
  end

  def y
    return @y
  end

  def i
    return @i
  end

  def g
    return @g
  end

  def h
    return @h
  end

  def f
    return @f
  end

  def set_g g
    @g = g
  end

  def set_h h
    @h = h
  end

  def set_f f
    @f = f
  end

end
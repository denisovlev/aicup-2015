require './model/car'
require './model/game'
require './model/move'
require './model/world'
require 'ostruct'
require 'matrix'

class MyStrategy
  # @param [Car] me
  # @param [World] world
  # @param [Game] game
  # @param [Move] move

  attr_reader :me
  attr_reader :world
  attr_reader :game
  attr_reader :my_move

  attr_reader :prev_coords
  attr_reader :cur_coords

  attr_reader :prev_tile
  attr_reader :cur_tile

  attr_reader :stuck_ticks

  attr_reader :is_reverse
  attr_reader :reverse_angle
  attr_reader :reverse_ticks
  attr_reader :last_speeds
  attr_reader :last_powers

  attr_accessor :path

  def wp_by_index(index)
    wp_raw = world.waypoints[index]
    make_tile(wp_raw[0], wp_raw[1])
  end

  def inc_wp_index(index, inc=1)
    (index + inc) % world.waypoints.length
  end

  def moved_dist
    return 100 if !cur_coords || !prev_coords
    Math.hypot(cur_coords.x-prev_coords.x, cur_coords.y-prev_coords.y)
    # 100
  end

  def stuck?
    return false if world.tick < game.initial_freeze_duration_ticks + 10
    return false unless last_speeds.length >= 10
    avg_speed = (last_speeds.reduce(0, &:+) / last_speeds.length)
    avg_power = (last_powers.reduce(0, &:+) / last_powers.length)
    avg_power > 0.3 && avg_speed < 0.15
  end

  def pi
    Math::PI
  end

  def my_tile_x
    dist_to_tiles(me.x).to_i
  end

  def my_tile_y
    dist_to_tiles(me.y).to_i
  end

  def my_tile_type
    world.tiles_x_y[my_tile_x][my_tile_y]
  end

  def closest(points, dest)
    return points.first if points.count == 1
    points.sort_by do |a|
      Math.hypot(a.x-dest.x, a.y-dest.y)
    end.first
  end

  def coord_equal(a, b)
    a.x == b.x && a.y == b.y
  end

  def tile_to_s(tile)
    return '(none, none)' unless tile
    "(#{tile.x},#{tile.y})"
  end

  def predict_tile(prev_tile, cur_tile, wp_tile_index)
    wp_tile = wp_by_index(wp_tile_index)
    neighbours = tile_neighbours(cur_tile)
    neighbours.select! do |tile| 
      # puts "current: #{tile_to_s(cur_tile)}, prev: #{tile_to_s(prev_tile)}, neigh: #{tile_to_s(tile)}"
      next false if tile.type == TileType::EMPTY  
      next false if coord_equal(tile, prev_tile) 
      non_passable = case cur_tile.type
      when TileType::VERTICAL
        [ tile_to_the(cur_tile, Direction::LEFT), tile_to_the(cur_tile, Direction::RIGHT) ]
      when TileType::HORIZONTAL 
        [ tile_to_the(cur_tile, Direction::UP), tile_to_the(cur_tile, Direction::DOWN) ]
      when TileType::LEFT_TOP_CORNER
        [ tile_to_the(cur_tile, Direction::LEFT), tile_to_the(cur_tile, Direction::UP) ]
      when TileType::RIGHT_TOP_CORNER
        [ tile_to_the(cur_tile, Direction::RIGHT), tile_to_the(cur_tile, Direction::UP) ]
      when TileType::LEFT_BOTTOM_CORNER
        [ tile_to_the(cur_tile, Direction::LEFT), tile_to_the(cur_tile, Direction::DOWN) ]
      when TileType::RIGHT_BOTTOM_CORNER
        [ tile_to_the(cur_tile, Direction::RIGHT), tile_to_the(cur_tile, Direction::DOWN) ]
      # when TileType::LEFT_HEADED_T  
      #   [ tile_to_the(cur_tile, Direction::RIGHT) ]
      # when TileType::RIGHT_HEADED_T
      #   [ tile_to_the(cur_tile, Direction::LEFT) ]
      # when TileType::TOP_HEADED_T
      #   [ tile_to_the(cur_tile, Direction::DOWN) ]
      # when TileType::BOTTOM_HEADED_T
      #   [ tile_to_the(cur_tile, Direction::UP) ]
      else
        []
      end
      next false if non_passable.any? { |np_tile| coord_equal(np_tile, tile) }
      true
    end
    # tile = closest(neighbours, wp_tile)
    if neighbours.length > 1
      current_path = get_path(cur_tile, wp_tile_index)
      tile = current_path[1]
      tile.type = tile_type(tile)
    else
      tile = neighbours.first
    end
    # current_path = get_path(cur_tile, wp_tile_index)
    # tile = current_path[1] 
    # puts "Current tile:#{tile_to_s(cur_tile)}, next:#{tile}"
    tile
  end

  def tile_center_coords(tile)
    x = (tile.x + 0.5) * game.track_tile_size
    y = (tile.y + 0.5) * game.track_tile_size
    make_coords(x, y)
  end

  def dist_tiles(n)
    game.track_tile_size * n
  end

  def dist_to_tiles(dist)
    dist / dist_tiles(1)
  end

  def deg_to_rad(n)
    Math::PI/180 * n
  end

  def coord_to_tile(coord)
    x = dist_to_tiles(coord.x).floor
    y = dist_to_tiles(coord.y).floor
    make_tile(x, y)
  end

  def make_tile(x, y)
    tile = make_coords(x, y)
    tile.type = tile_type(tile)
    tile
  end

  def project_vector(coord0, length, angle)
    x = coord0.x + length * Math.cos(angle)
    y = coord0.y + length * Math.sin(angle)
    make_coords(x, y)
  end

  def get_my_front_tile
    coord_to_tile(get_my_front_coord)
  end

  def get_my_front_coord
    center = make_coords(me.x, me.y)
    length = me.height / 2
    angle = me.angle
    front_coord = project_vector(center, length, angle)
  end

  def direction_to_coord(direction)
    case direction
    when Direction::LEFT
      make_coords(-1, 0)
    when Direction::RIGHT
      make_coords(1, 0)
    when Direction::UP
      make_coords(0, -1)
    when Direction::DOWN
      make_coords(0, 1)
    end
  end

  def reverse_direction(direction)
    case direction
    when Direction::LEFT
      Direction::RIGHT
    when Direction::RIGHT
      Direction::LEFT
    when Direction::UP
      Direction::DOWN
    when Direction::DOWN
      Direction::UP
    end
  end

  def add_coord(coord1, coord2)
    make_coords(coord1.x + coord2.x, coord1.y + coord2.y)
  end

  def tile_to_the(start_tile, direction)
    tile = add_coord(start_tile, direction_to_coord(direction))
    tile.type = tile_type(tile)
    tile
  end

  def get_path(current, next_wp_index)
    goal =  wp_by_index(next_wp_index)
    next_goal = wp_by_index(inc_wp_index(next_wp_index))

    if path && path.last == next_goal
      current_index = path.index(current)
      if current_index
        return path.slice(current_index..path.length)
      end
    end

    came_from = astar(@world.tiles_x_y, current, goal)
    path = reconstruct_path(came_from, current, goal)

    came_from = astar(@world.tiles_x_y, goal, next_goal)
    path += reconstruct_path(came_from, goal, next_goal).drop(1)
  end

  def register_move
    @last_speeds ||= []
    @last_powers ||= []
    if last_speeds.length > 10
      @last_speeds.pop
      @last_powers.pop
    end
    cur_speed = Math.hypot(me.speed_x, me.speed_y)
    cur_speed = 1 if @state == :reverse
    @last_speeds.unshift(cur_speed) unless me.durability <= 0
    @last_powers.unshift(me.engine_power)

    cur = make_coords(me.x, me.y)
    @prev_coords = cur_coords
    @cur_coords = cur
    front_tile = get_my_front_tile
    if !prev_tile && !cur_tile
      direction = world.starting_direction
      @prev_tile = tile_to_the(front_tile, reverse_direction(direction))
      @cur_tile = front_tile
    end
    @prev_tile = cur_tile if !cur_tile || !coord_equal(cur_tile, front_tile)
    @cur_tile = front_tile
  end

  def calc_waypoint
    next_waypoint_x = get_next_waypoint_x
    next_waypoint_y = get_next_waypoint_y
    return next_waypoint_x, next_waypoint_y
  end

  def angle_to(coord0, angle, coord1)
    absolute_angle_to = Math::atan2(coord1.y - coord0.y, coord1.x - coord0.x)
    relative_angle_to = absolute_angle_to - angle

    while relative_angle_to > Math::PI
      relative_angle_to -= 2.0 * Math::PI
    end

    while relative_angle_to < -Math::PI
      relative_angle_to += 2.0 * Math::PI
    end

    relative_angle_to
  end

  def distance_between(coord1, coord2)
    Math.hypot(coord1.x-coord2.x, coord1.y-coord2.y)
  end

  def move(me, world, game, move)
    @me = me
    @world = world
    @game = game
    @my_move = move

    register_move
    # puts "stuck: #{stuck?}, moved: #{moved_dist}"
    # if stuck? || in_reverse_mode?
    #   return recover_from_stop
    # end

    wp_index = me.next_waypoint_index
    # wp = wp_by_index(wp_index)

    next_tile = predict_tile(prev_tile, cur_tile, wp_index)
    # if coord_equal(next_tile, wp)
    #   wp_index = inc_wp_index(wp_index)
    # end

    after_next_tile = predict_tile(cur_tile, next_tile, wp_index)

    # puts "Current tile:#{tile_to_s(cur_tile)}, next:#{tile_to_s(next_tile)}, after next:#{tile_to_s(after_next_tile)}"

    my_front = get_my_front_coord
    my_angle = me.angle
    speed_module = Math.hypot(me.speed_x, me.speed_y)
    my_next_coord = make_coords(me.x, me.y) + make_coords(me.speed_x, me.speed_y)

    unless speed_module > 7
      after_next_tile = next_tile
    end

    goal_coords = tile_center_coords(after_next_tile)

    safe_tiles = [TileType::VERTICAL, TileType::HORIZONTAL]
    bonuses = world.bonuses.sort_by do |bonus|
      [
        BonusType::PURE_SCORE, BonusType::REPAIR_KIT,
        BonusType::AMMO_CRATE, BonusType::OIL_CANISTER,
        BonusType::NITRO_BOOST
      ].index(bonus.type)
    end
    bonuses.each do |bonus|
      bonus_tile = coord_to_tile(bonus)
      next unless bonus_tile == after_next_tile || bonus_tile == next_tile 
      next unless safe_tiles.include?(bonus_tile.type)
      goal_coords = bonus
      break
    end
    
    angle_to_waypoint = angle_to(my_next_coord, my_angle, goal_coords) 
    # angle_to_waypoint = me.angle_to(next_waypoint_x, next_waypoint_y) 

    # wp_index = world.waypoints.find_index {|tile| coord_equal(make_coords(tile[0], tile[1]), wp_tile)} 
    # if wp_index >= 22
    #   puts "Current tile:#{tile_to_s(cur_tile)}, next:#{tile_to_s(next_tile)}, af next:#{tile_to_s(after_next_tile)}, wp:(#{tile_to_s(wp_tile)})"
    # end

    speed_module_sigma = 1/(1 + Math::exp(speed_module))
    turn = angle_to_waypoint + (angle_to_waypoint * Math::cos(speed_module_sigma) / 2)
    sigma_angle = 2/(1 + Math.exp(-3 * (angle_to_waypoint.abs/Math::PI) )) 
    move.engine_power = Math.cos(sigma_angle) + 0.5

    if turn.abs >= Math::PI/4
      # puts speed_module.round(2)
      if speed_module >= 20
        move.brake = true
      end
    end

    if turn.abs >= Math::PI/2.5
      # puts speed_module.round(2)
      if speed_module >= 10
        move.brake = true
      end
    end

    # puts last_powers, last_powers.class
    # if last_speeds.length >= 3 && last_speeds[0] - last_speeds[1] > 0 && last_speeds[1] - last_speeds[2] > 0
    #   move.use_nitro = true
    # end

    # if distance_to_waypoint > dist_tiles(4)
    #   my_move.use_nitro = true
    # end

    # magic = 10

    # if speed_module * speed_module * angle_to_waypoint.abs > magic * magic * pi/2
    #   move.engine_power = 0
    #   move.brake = true
    # end

    # if me.angular_speed >= Math::PI * 0.2
    #   move.engine_power = 0.5
    # end

    # if me.angular_speed >= Math::PI * 0.05
    #   move.brake = true
    # end

    # puts me.engine_power.round(2)
    # puts speed_module.round(2)

    if stuck? 
      @state = :reverse 
      @reverse_start = make_coords(me.x, me.y)
    end

    if @state == :reverse
      move.engine_power = -1
      # move.wheel_turn = reverse_angle
      @reversed ||= 0
      @reversed += 1
      if @reversed >= 90 || distance_between(make_coords(me.x, me.y), @reverse_start) > 100
        @state = :going
        @reversed = 0  
      end
    end


    if me.engine_power < 0
      turn = -turn * 1.5
    end
    move.wheel_turn = turn

    attack  
    # pick_bonuses
    # avoid_obstacle
  end

  def make_coords(x, y)
    Vector[x, y]
  end

  def tile_neighbours(tile)
    [
      make_coords(tile.x + 1, tile.y),
      make_coords(tile.x, tile.y + 1),
      make_coords(tile.x - 1, tile.y),
      make_coords(tile.x, tile.y - 1),
    ].map do |tile|
      tile.type = tile_type(tile)
      tile
    end
  end

  def tile_type(tile)
    return TileType::EMPTY if tile.x < 0 || tile.x > world.width - 1 || tile.y < 0 || tile.y > world.height - 1
    world.tiles_x_y[tile.x][tile.y]
  end

  def avoid_obstacle
    x = me.x
    y = me.y
    speed_x = me.speed_x
    speed_y = me.speed_y
    next_tile_x = dist_to_tiles(x + speed_x)
    next_tile_y = dist_to_tiles(y + speed_y)
    next_tile_type = world.tiles_x_y[next_tile_x][next_tile_y]
    return unless next_tile_type == TileType::EMPTY

    my_tile_x = dist_to_tiles(x)
    my_tile_y = dist_to_tiles(y)

    neighbours = tile_neighbours(make_coords(my_tile_x, my_tile_y))
    not_empty_neighbours = neighbours.select {|tile| tile.type != TileType::EMPTY}
    not_empty_neighbours.each do |tile|
      wp_x = me.next_waypoint_x
      wp_y = me.next_waypoint_y
      dist_x = wp_x - tile.x
      dist_y = wp_y - tile.y
      dist = Math.hypot(dist_x, dist_y)
      tile.dist_to_wp = dist
    end
    not_empty_neighbours.sort! do |a, b|
      a.dist - b.dist
    end
    closest = not_empty_neighbours.first
    my_move.wheel_turn = me.angle_to(closest.x, closest.y)
  end

  def attack
    world.cars.each do |car|
      next if car.teammate
      distance = me.get_distance_to_unit(car)
      next if distance > attack_distance
      angle = me.get_angle_to_unit(car)
      if angle.abs < attack_washer_angle
        my_move.throw_projectile = true
      end
      if angle.abs > attack_oil_angle
        my_move.spill_oil = true
      end
    end
  end

  def attack_distance
    dist_tiles(3)
  end

  def attack_washer_angle
    deg_to_rad(3)
  end 

  def attack_oil_angle
    pi - (pi/180 * 5)
  end

  def reconstruct_path(came_from, start, goal)
    current = goal
    path = [current]

    while current != start
      current = came_from[current]
      path << current
    end

    path.reverse
  end

  def astar(graph, start, goal)
    frontier = PriorityQueue.new
    frontier.put(start, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start] = nil
    cost_so_far[start] = 0

    until frontier.empty?
      current = frontier.get

      # break if current == goal

      neighbors(graph, current).each do |neighbor|
          new_cost = cost_so_far[current] + cost(graph, current, neighbor)
          if !cost_so_far[neighbor] || new_cost < cost_so_far[neighbor]
              cost_so_far[neighbor] = new_cost
              priority = new_cost + heuristic(graph, goal, neighbor)
              frontier.put(neighbor, priority)
              came_from[neighbor] = current
          end
      end
    end

    came_from
  end

  def cost(graph, current, neighbor)
    1
  end

  def heuristic(graph, goal, neighbor)
    # Manhattan distance on a square grid
    (goal[0] - neighbor[0]).abs + (goal[1] - neighbor[1]).abs
  end

  def neighbors(graph, node)
    neighbors = available_paths(graph, node)
    result = []

    result << node + Vector[0, -1] if neighbors.include? :top
    result << node + Vector[0, 1] if neighbors.include? :bottom
    result << node + Vector[-1, 0] if neighbors.include? :left
    result << node + Vector[1, 0] if neighbors.include? :right

    result
  end

  def available_paths(graph, start_node)
    current_type = graph[start_node[0]][start_node[1]]
    return [] if current_type == 0

    result = []

    result << :top if [1, 5, 6, 7, 8, 9, 11].include? current_type
    result << :bottom if [1, 3, 4, 7, 8, 10, 11].include? current_type
    result << :left if [2, 4, 6, 7, 9, 10, 11].include? current_type
    result << :right if [2, 3, 5, 8, 9, 10, 11].include? current_type

    # VERTICAL = 1
    # HORIZONTAL = 2
    # LEFT_TOP_CORNER = 3
    # RIGHT_TOP_CORNER = 4
    # LEFT_BOTTOM_CORNER = 5
    # RIGHT_BOTTOM_CORNER = 6
    # LEFT_HEADED_T = 7
    # RIGHT_HEADED_T = 8
    # TOP_HEADED_T = 9
    # BOTTOM_HEADED_T = 10
    # CROSSROADS = 11

    result
  end
end

class PriorityQueue
  def initialize
    @elements = []
  end

  def put(element, priority)
    @elements << [ element, priority ]
    @elements = @elements.sort_by { |e| e[1] }.reverse
  end

  def get
    @elements.pop[0]
  end

  def empty?
    @elements.empty?
  end
end

class Vector
  def x
    self[0]
  end

  def y
    self[1]
  end

  attr_accessor :type
end 
